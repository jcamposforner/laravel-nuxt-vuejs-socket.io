<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientsFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:clients',
            'state' => 'required|string',
            'type' => 'required|string',
            'credito' => 'required|string',
            'limite' => 'required|string',
            'nodo' => 'required|string',
            'cantidad' => 'required|integer',
        ];
    }
}
