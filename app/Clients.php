<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    protected $table = 'clients';

    public $primaryKey = 'id';

    public $timestamps = true;
}
