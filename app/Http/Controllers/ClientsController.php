<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ClientsFormRequest;
use App\Clients;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Clients::all();
        return response([
            'status' => 'success',
            'clients' => $clients
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientsFormRequest $request)
    {
        $client = new Clients;
        $client->name = $request->name;
        $client->state = $request->state;
        $client->type = $request->type;
        $client->credito = $request->credito;
        $client->limite = $request->limite;
        $client->nodo = $request->nodo;
        $client->cantidad = $request->cantidad;
        $client->save();

        return response([
            'status' => 'success',
            'data' => $client
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Clients::find($id);
        return response([
            'status' => 'success',
            'clients' => $client
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $client = Clients::find($id);
        $client->name = $request->name;
        $client->state = $request->state;
        $client->type = $request->type;
        $client->credito = $request->credito;
        $client->limite = $request->limite;
        $client->nodo = $request->nodo;
        $client->cantidad = $request->cantidad;
        $client->save();

        return response([
            'status' => 'success',
            'data' => $client
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Clients::find($id);
        $client->delete();

        return response([
            'status' => 'success',
            'data'  => $client
        ]);
    }
}
