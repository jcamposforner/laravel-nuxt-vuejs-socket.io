<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterFormRequest;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use JWTFactory;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Payload;

class AuthController extends Controller
{
    public function register(RegisterFormRequest $request)
    {
        $user = new User;
        $user->email = $request->email;
        $user->name = $request->name;
        $user->password = bcrypt($request->password);
        $user->save();
        return response([
            'status' => 'success',
            'data' => $user
        ], 200);
    }

    public function login(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');
        $user = User::where([
            'email'=>$credentials['email'],
        ])->first();

        $a = $user['password'];
        if(!$user){
            return response([
                'status' => 'fail',
                'message' => 'No existe el usuario'
            ],404);
        }

        if(Hash::check($credentials['password'],$user['password'])) {
            $token = JWTAuth::fromUser($user);
            
            if ($token) {
                return response()->json([
                    'token' => 'Bearer '.$token,
                    'user' => $user,
                ]);
            } else {
                return response()->json([
                    'code' => 2,
                    'message' => 'Invalid credentials.'
                ], 401);
            }
        }else {
            return response()->json([
                'code' => 2,
                'message' => 'Invalid credentials.'
            ], 401);
        }
    }
}
