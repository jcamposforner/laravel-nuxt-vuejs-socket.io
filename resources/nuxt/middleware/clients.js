
export default function ( { app, params, store } ) {
    /*
    Cuando funcione login para cambiar las cabeceras de todas las peticiones axios
    console.log(app.$axios.defaults.headers.common);
    */
    return app.$axios.$get('api/clients', { headers : {
        Authorization:'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMVwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU0NjI2NTk1OCwiZXhwIjoxNTQ2ODcwNzU4LCJuYmYiOjE1NDYyNjU5NTgsImp0aSI6Ik9DeG1aYUcwTXgxYmpad08iLCJzdWIiOjIsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEiLCJuYW1lIjoiSmVzdSIsImVtYWlsIjoiamVzdXMuYWx1QGdtYWlsLmNvbSJ9.ineMcfS6XAuA76DW5wlX117iHcS5FgxsTffr_bm3BRU'
    }})
        .then((response) =>{
            if(response.status === 'success'){
                store.commit('clients/showClients', response.clients);
            }else{
                console.log(response.status);
            }
        },(err) => {
            console.log(err);
        });

}