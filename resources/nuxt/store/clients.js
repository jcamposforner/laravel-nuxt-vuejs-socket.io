export const state = () => ({
    clients: [],
  })

export const mutations = {
    showClients (state, payload) {
        state.clients = payload;
    },
    updateClient ( state, payload) {
       
        this.$axios.$put(`/api/clients/${payload.id}`, payload, { headers : {
            Authorization:'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMVwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU0NjI2NTk1OCwiZXhwIjoxNTQ2ODcwNzU4LCJuYmYiOjE1NDYyNjU5NTgsImp0aSI6Ik9DeG1aYUcwTXgxYmpad08iLCJzdWIiOjIsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEiLCJuYW1lIjoiSmVzdSIsImVtYWlsIjoiamVzdXMuYWx1QGdtYWlsLmNvbSJ9.ineMcfS6XAuA76DW5wlX117iHcS5FgxsTffr_bm3BRU'
        }})
    },
    deleteClient ( state, payload ) {
        this.$axios.$delete(`/api/clients/${payload}`, { headers : {
            Authorization:'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMVwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU0NjI2NTk1OCwiZXhwIjoxNTQ2ODcwNzU4LCJuYmYiOjE1NDYyNjU5NTgsImp0aSI6Ik9DeG1aYUcwTXgxYmpad08iLCJzdWIiOjIsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEiLCJuYW1lIjoiSmVzdSIsImVtYWlsIjoiamVzdXMuYWx1QGdtYWlsLmNvbSJ9.ineMcfS6XAuA76DW5wlX117iHcS5FgxsTffr_bm3BRU'
        }})
    }
}